
public class MexicanPizza extends Pizza {
	public MexicanPizza() {
		name = "Mexican Pizza";
		dough = "Mexican crust";
		sauce = "Mexican sauce";
		toppings.add("Mexican things");
		toppings.add("Jalapeño");
	}
}
