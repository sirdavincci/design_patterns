
public class HawaianPizza extends Pizza {
	public HawaianPizza() {
		name = "Pizza Hawaiana";
		dough = "Súper Delgada";
		sauce = "Salsa BBQ";
		toppings.add("Jamon");
		toppings.add("Piña");
	}
}
