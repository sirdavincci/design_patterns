
public class BeverageTestDrive {
	public static void main(String[] args) {
 
		Tea tea = new Tea();
		Coffee coffee = new Coffee();
        
        coffee.prepareRecipe();
        System.out.println();
        tea.prepareRecipe();
        System.out.println();
		
	}
}
